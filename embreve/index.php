<!doctype html>
<html lang="pt-BR" prefix="og: http://ogp.me/ns#">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0" />	
	<meta name="description" content="Ponto Brasil - Escritório inteligente - Coworking em Americana">
	<link rel="dns-prefetch" href="//fonts.googleapis.com">
	<link rel="dns-prefetch" href="//google-analytics.com">
	<title>Ponto Brasil - Escritório inteligente - Coworking em Americana</title>

	<meta name="description" content="Temos como valores Inovação, Cooperação, Qualidade, Sustentabilidade e Equilíbrio. O ambiente é calmo, grande parte construída de maneira sustentável, local zen e em ponto estratégico da cidade"/>
	<link rel="canonical" href="https://pontobrasil.com.br/" />
	<meta property="og:locale" content="pt_BR" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="Ponto Brasil - Escritório inteligente - Coworking em Americana" />
	<meta property="og:description" content="Temos como valores Inovação, Cooperação, Qualidade, Sustentabilidade e Equilíbrio. O ambiente é calmo, grande parte construída de maneira sustentável, local zen e em ponto estratégico da cidade" />
	<meta property="og:url" content="https://pontobrasil.com.br/" />
	<meta property="og:site_name" content="Ponto Brasil" />
	<meta property="og:image" content="https://pontobrasil.com.br/img/Ponto-Brasil-Escritorio.jpg" />
	<meta property="og:image:width" content="1920" />
	<meta property="og:image:height" content="1047" />
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:description" content="Temos como valores Inovação, Cooperação, Qualidade, Sustentabilidade e Equilíbrio. O ambiente é calmo, grande parte construída de maneira sustentável, local zen e em ponto estratégico da cidade" />
	<meta name="twitter:title" content="Ponto Brasil - Escritório inteligente - Coworking em Americana" />
	<meta name="twitter:site" content="@PtoBR" />
	<meta name="twitter:image" content="https://pontobrasil.com.br/img/Ponto-Brasil-Escritorio.jpg" />
	<meta name="twitter:creator" content="@PtoBR" />
	<script type='application/ld+json'>{"@context":"https:\/\/schema.org","@type":"WebSite","@id":"#website","url":"https:\/\/pontobrasil.com.br\/","name":"Ponto Brasil","query-input":"required name=search_term_string"}}</script>
	
	<link href="https://fonts.googleapis.com/css?family=Roboto|Montserrat:400,500,600|Material+Icons" rel="stylesheet">	
	<link rel="stylesheet" type="text/css" href="slick/slick.css"/>	

	<style type="text/css">

		body{
			height: 100%;
			margin: 0;
			font-family: 'Roboto', sans-serif;		
		}

		p{
			font-size: 18px;		
		}

		ul{
			list-style: none;
			padding: 0;
		}

		.container{ overflow-x: hidden!important; }


		.box-left{			
			width: 50%;		
			float: left;
		}

		.content{
			padding: 50px 25px;
		}

		.title{
			text-align: center;
		}

		.title img{
			width: 170px;
		}

		.anchor_nav {
			font-family: 'Montserrat', sans-serif;
			font-size: 18px;
			font-weight: 600;
			text-decoration: none;
			color: #005aa9;
		}

		.anchor_nav span{
			padding: 0 25px;
		}

		.anchor_nav{
			margin: 40px 0;
		}

		.box-right{
			background-color: #fff;
			width: 50%;
			height: 100%;
			position: fixed;
			right: 0;
		}

		.informacoes{
			text-align: center;		
		}		

		.emailform{
			border:0;
			background: #ffd300;
			color: #464646;
			padding: 14px;
			border-radius: 3px;
			font-size: 14px;
			width: 237px;
			font-weight: 700;
		}

		.cadastrarform{
			border: 0;
			padding: 14px;
			border-radius: 3px;
			background: #004b4c;
			color: #ffffff;
			font-size: 14px;
			font-weight: 700;
			cursor: pointer;
		}

		.cadastrarform:hover{
			opacity: 0.8;			
			background: #00944d;
			transition: transform 0.5s,
			opacity 0.5s,
			background-color 0.5s;
		}

		.formulario{
			margin-bottom: 30px;
		}

		.sobre_content,
		.planos_content{
			margin-bottom: 70px;			
		}

		.sobre_content h1,
		.planos_content h1,
		.formulario h1{
			font-size: 20px;
			font-weight: bold;
			text-transform: uppercase;
			border-bottom: 1px solid #ffd300;
		}

		.sobre_content p,
		.planos_content p,
		.formulario p{
			line-height: 26px;
			font-size: 16px;
		}

		.planos_content li{
			padding-bottom: 20px;
		}

		.contato{
			font-family: 'Montserrat', sans-serif;
			font-weight: 500;
			margin-bottom: 70px;		
		}

		.tel{
			margin-bottom: 40px;
		}

		.tel p{
			font-size: 16px;
			margin: 0;
			color: #005aa9;
		}

		.tel a{
			text-decoration: none;
			font-size: 24px;
			color: #005aa9;
		}	

	    .local p{
	    	margin: 0;
	    }

		.local a{
			color: #a1a1a1;
			text-decoration: none;
			font-size: 16px;
		}

		.group_left{
			float: left;
			margin-right: 24px;
		}

		.sobre_icons i{
			padding-right: 8px;
		}

		.sobre_icons .group_planos_left{
			padding-right: 20px;
		}

		ul {
			display: block;
		}

		ul li {
			width: 45%;
			float: left;
			padding-right: 10px;
		}

		ul li span { vertical-align: super }
		
		.group_planos_left,
		.group_planos_right{		
			width: 50%;
			padding-right: 10px;
		}

		.material-icons { color: #d6de23 }

		.flex{
			display: flex;
			display: -webkit-flex;
		}

		.social img{
			padding-right: 5px;
		}	

		@media screen and (max-width: 769px) {

			.box-left{
				width: 100%;
				float: none;
			}

			.box-right{
				display: none;
			}

		}


		@media screen and (max-width: 425px) {

			.bullet { 
				width: 100%;
			    display: block;
			    text-indent: -99999px;
			    line-height: 4px;
			    color: #fff;
		  	}

		 	ul li { 
		  		width: 100%; 
		  	}
		}

		@media screen and (max-width: 375px) {

			.emailform { 
				width: 100%; 
				margin-bottom: 15px;
			}

		}

	</style>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-1032165-33"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-1032165-33');
	</script>

</head>

<body>	
	<div class="container">
			
		<div class="box-left">

			<div class="content">

				<div class="title">
					
					<img src="img/ponto-brasil.png">			

					<nav class="anchor_nav">
						Custo X Benefício
						<span class="bullet">&#8226;</span>
						Ótima Localização
						<span class="bullet">&#8226;</span>
						Espaço Gourmet
					</nav>

				</div>

				<div class="informacoes">	

					<div class="contato">
						<div class="tel">
							<p>Reserve sua sala</p>
							<a href="tel:+551936041009">(19) 3604.1009</a>
						</div>
						
						<div class="local">
							<p>Ponto Brasil escritório inteligente</p>
							<a href="https://goo.gl/gQRE8g" target="_blank">Rua Amélio Ettore Gobbo, 113. Jardim Paulista, Americana-SP</a>
						</div>										
					</div>
								
				</div>

				<div class="sobre_content">

					<h1>Sobre</h1>

					<p>O coworking Ponto Brasil, situado na cidade de Americana, oferece espaço e serviços para profissionais visionários que querem sempre estar à frente com as inovações da economia moderna e suas novas formas de trabalhar. Somos pioneiros na cidade, sendo o primeiro coworking em Americana.
					<br><br>
					Oferecemos soluções para diferentes perfis de profissionais.
					<br><br>
					Para quem faz home office,  mas não tem um endereço comercial para divulgar, atender seus clientes e receber correspondências, conseguimos oferecer um escritório completo,  inclusive com recepção para estas pessoas divulgarem seu negócio, sem precisarem desembolsar um alto custo para terem um escritório em uma sala comercial convencional.
					<br><br>
					Da mesma forma temos estrutura e serviços para profissionais e empresas que tenham um escritório em sala comercial convencional reduzam seus custos.
					<br><br>
					Disponibilizamos salas com estações de trabalho e armário privativo,  onde empreendedores podem ter seu escritório completo com todos os serviços já inclusos com um custo mensal até 6x menor e com zero investimento,  comparado se tivesse um escritório convencional.
					<br><br>
					Temos também sala comercial em Americana, em nossa sede, para empresas que tem uma equipe trabalhem em sinergia.
					<br><br>
					Contamos com sala de reunião e sala de atendimento para diversos tipos de empresas e profissionais fazerem uma reunião de negócios ou uma sessão de atendimento.
					<br><br>
					Temos um auditório completo para cursos, palestras, treinamentos...
					<br><br>
					Ocupamos uma área de aproximadamente 1200 metros quadrados de terreno, sendo 300 metros quadrados construídos, com amplo estacionamento e localização privilegiada na cidade de Americana.
					<br><br>
					Todos os planos já incluem as despesas com Energia, Água, IPTU, Gestão e Manutenção predial. Além disto, também incluem Estacionamento amplo, Ótima localização, Internet de fibra óptica de alta velocidade com 2 links redundantes, Ar condicionado, Lounge para recepção, descanso e coffee break, Copa completa, Refeitório, Limpeza, Água mineral, Café (em dias comerciais), Segurança com sistema de câmeras, alarme e cerca elétrica.
					<br><br>
					O ambiente foi montado com soluções sustentáveis e as plantas tornam o ambiente bem descontraído e aconchegante.</p>
			
					<ul class="sobre_icons"> 
						<li><i class="material-icons">lock_outline</i> <span>Armário privado</span></li>
						<li><i class="material-icons">attach_file</i> <span>Material de escritório</span></li>
						<li><i class="material-icons">people_outline</i> <span>Espaço para convivência</span></li>
						<li><i class="material-icons">restaurant</i> <span>Cozinha/copa</span></li>
						<li><i class="material-icons">local_cafe</i> <span>Café grátis</span></li>
						<li><i class="material-icons">group</i> <span>Sala de reuniões</span></li>
						<li><i class="material-icons">mail_outline</i> <span>Endereço para correspondência</span></li>
						<li><i class="material-icons">print</i>Serviço de impressão</li>
						<li><i class="material-icons">content_paste</i>Serviço de secretariado</li>
						<li><i class="material-icons">local_phone</i>Telefone privado</li>
						<li><i class="material-icons">wifi</i>Internet até 100MBs</li>
						<li><i class="material-icons">accessible</i>Acessível para cadeirante</li>
						<li><i class="material-icons">local_parking</i>Estacionamento privado</li>
						<li><i class="material-icons">payment</i>Aceita cartões de crédito/débido</li>	
					</ul>
				
					<br clear="all">
				</div>
				
				<div class="planos_content">
					<h1>Planos</h1>
					<p>O Ponto Brasil oferece 9 planos diferentes.</p>

					<ul>
					    <li><strong>Estações de trabalho</strong> com qualidade.</li>
					    <li><strong>Sala de reunião</strong> com TV, lousa e ar condicionado.</li>
						<li><strong>Auditório</strong> para 30 pessoas.</li>					    						   
					    <li><strong>Sala comercial exclusiva</strong> e mobiliada.</li>				
					    <li><strong>Endereço Comercial</strong> para divulgar sua empresa.</li> 				
					    <li><strong>Endereço Fiscal</strong> para registrar sua empresa.</li>
					    <li><strong>Telefonia IP</strong> com número fixo de telefone.</li>					
					    <li><strong>Secretária Virtual</strong> para atender seus telefonemas.</li>
					    <li><strong>Escritório Virtual</strong> com endereço + secretária virtual.</li>					
				   </ul>
				   <br clear="all">
				</div>			
				
				<div class="formulario">
					<h1>Fique por dentro</h1>
					<p>Cadastre-se em nossa newsletter e fique por dentro das novidades de Ponto Brasil, escritório inteligente.</p>

					<div id="mc_embed_signup">
						<form action="https://pontobrasil.us19.list-manage.com/subscribe/post?u=67cacc4210fcf431b46d9aac9&amp;id=77609a8407" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
						    <div id="mc_embed_signup_scroll">					
								<input type="email" value="" name="EMAIL" class="email emailform" id="mce-EMAIL" placeholder="Seu email" required>
								<input type="submit" value="Cadastrar" name="subscribe" id="mc-embedded-subscribe" class="button cadastrarform">					    	
						    	<div style="position: absolute; left: -5000px;" aria-hidden="true">
						    		<input type="text" name="b_67cacc4210fcf431b46d9aac9_77609a8407" tabindex="-1" value="">
						    	</div>
						    	<div class="clear"></div>
						    </div>
						</form>
					</div>
				</div>

				<div class="social">
					<a href="https://www.facebook.com/PtoBR" title="Facebook Ponto Brasil" target="_blank"><img src='img/facebook.png' title='Facebook Ponto Brasil'></a>	

					<a href="https://twitter.com/PtoBR" title="Twitter Ponto Brasil" target="_blank"><img src='img/twitter.png' title='Twitter Ponto Brasil'></a>	

					<a href="https://plus.google.com/u/0/+PontoBrasil" title="Google Plus Ponto Brasil" target="_blank"><img src='img/gplus.png' title='Google Plus Ponto Brasil'></a>	

					<a href="https://www.youtube.com/user/pntbr" title="Youtube Ponto Brasil" target="_blank"><img src='img/youtube.png' title='Youtube Ponto Brasil'></a>					
				</div>	

			</div>

		</div>

		<div class="box-right">
		<?php

		//Imagens do slider
		$slider_img = array(
			"img/ponto-brasil-001.jpg",
			"img/ponto-brasil-002.jpg",
			"img/ponto-brasil-003.jpg",
			"img/ponto-brasil-004.jpg"
		); 

			$i = 0;
			foreach($slider_img as $pimg)
			{
			?>
			    <div class="active item" data-slide-number="<?php echo $i; ?>">
			        <img src="<?php echo $pimg; ?>">
			    </div>
			<?php   
			$i++;
			}
		?>
		</div>

		<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
		<script src="./slick/slick.js" type="text/javascript" charset="utf-8"></script>

		<script type="text/javascript">
		    $(document).ready(function(){
		      $('.box-right').slick({
		      	fade:true,
		      	autoplay: true,
		      	autoplaySpeed: 3500,
		        dots:false,
		        arrows:false,
		        speed: 500,
		        cssEase: 'linear'
		      });
		    });

		</script>

	</div>
</body>