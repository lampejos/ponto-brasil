# README #

* Site Ponto Brasil
* https://pontobrasil.com.br/

### Backup ###

* 15/Setembro/2021
* WordPress (full)
* MySQL
* ACF

### WordPress ###

* Versão 5.8.1

### Plugins ###

* Advanced Custom Fields PRO - 5.10.2
* Contact Form 7 - 5.4.2
* Flamingo - 2.2.2
* GoCache - CDN - 1.2.5
* Honeypot for Contact Form 7 - 2.1
* iThemes Security - 8.0.2
* Log cleaner for iThemes Security - 1.3.5
* Post SMTP - 2.0.23
* reCaptcha by BestWebSoft - 1.64
* Redirection for Contact Form 7 - 2.3.5
* TablePress - 1.14
* UpdraftPlus - Backup/Restore - 1.16.61
* Yoast SEO - 17.1

