<?php get_header(); ?> 

	<main class="main page-e404" role="main">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<div class="container-fluid">

				<div class="row">

					<div class="content-center content-page">
		    
			        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			        		<h1 class="txt-center">Página não encontrada</h1>

			        		<p>Navegue através do menu superior para conhecer nosso site!</p>
			        		
			        	</div>	

				    </div>

				</div>

			</div>	

		<?php endwhile; endif; ?>

	</main>

<?php get_footer(); ?>