<?php get_header(); ?> 

	<main class="main" role="main">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
			<?php the_title(); ?>
			<?php the_content(); ?>
			
			</div>
			
		<?php endwhile; endif; ?>

	</main>

<?php get_footer(); ?>