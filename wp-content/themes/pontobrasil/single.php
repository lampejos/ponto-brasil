<?php get_header(); ?> 

	<main class="main" role="main">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<div class="container-fluid">

					<div class="row">

						<img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), ''); ?>" class="single_thumb_full">						
					    
				    	<div class="content-center content-single">		

				        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				        		<?php
			        			echo '<div class="flex just-center align-center">';
	        				  		foreach((get_the_category()) as $category) { 	        			        		
										echo '<span class="post-categories">' .$category->cat_name. '</span>'; 
	        			        	}
	        			        echo '</div>';
				        		?>

				        		<h1 class="txt-center"><?php the_title(); ?></h1>

				        		<?php the_content(); ?>				        			

			        			<?php

			        			echo '<div class="row blog__thumb__post_related align-start">';
			        				echo '<h2 class="col-xs-12 col-sm-12 col-md-12 col-lg-12 txt-center">Assuntos Relacionados</h2>';

			        				$currentpostID = get_the_ID();
			        				$currentpostCat = get_the_category($currentpostID);
			        				
			        				if($currentpostCat[0]->category_count <= 1) {

		        						$the_categories = '';
			        				} 

			        				else{

			        					$the_categories = array($currentpostCat[0]->cat_ID);			
			        				}

	        						$array_posts = array ( 
	        							'post_type'			=> 'post',
	        						  	'posts_per_page'	=> 3,
	        						  	'category__in'      => $the_categories,
	        						  	'post__not_in'		=> array($currentpostID)
	        						);

	        						$blog_post = new WP_Query( $array_posts );

	        						if ( $blog_post->have_posts() ):
	        						    while ( $blog_post->have_posts() ) :
	        						    	$blog_post->the_post();	        						    	
	        						    	echo '<a href="'.get_permalink().'" class="col-xs-12 col-sm-12 col-md-4 col-lg-4 flex direction-col just-center pd-b_60 blog__thumb__single_related">';
        									  	 

        									  	echo '<div class="zoom_img">';        									  	
										  			echo '<img src="' . get_the_post_thumbnail_url(get_the_ID(), 'thumbnail') . '">';
										  		echo '</div>';

			  				        			echo '<div class="flex">';
			  		        				  		foreach((get_the_category()) as $category) { 	        			        		
			  											echo '<span class="post-categories">' .$category->cat_name. '</span>'; 
			  		        			        	}
			  		        			        echo '</div>';

										  		echo '<h3>';
										  			the_title();
										  		echo '</h3>';

										  		echo '<div class="flex align-end">';
										        	echo '<div class="button-sm blue br_blue flex align-center just-center">Leia Mais</div>';
									        	echo '</div>';

											echo '</a>';				  

	        						    endwhile;
	        						endif;

	        					echo '</div>';

			        			?>

				        	</div>	

					    </div>

					</div>

					<?php  include('dist/slickslider/planos.php');  ?>

				</div>			
			
			</div>
			
		<?php endwhile; endif; ?>

	</main>

<?php get_footer(); ?>