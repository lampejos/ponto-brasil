<?php




if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

		// Css Exit Popup #usePrimicia
		wp_register_style('exitpoup', get_template_directory_uri() . '/dist/exit_popup/stick-to-me.css', array(), '1.0', 'all');
		wp_enqueue_style('exitpoup');

		// Javascript Exit Popup #usePrimicia
		wp_register_script('exitpoup', get_template_directory_uri() . '/dist/exit_popup/stick-to-me.js', array(), '3', 'all');
		wp_enqueue_script('exitpoup');
}

add_action('customize_register', 'lampejos__customize__exitpopup');
function lampejos__customize__exitpopup($wp_customize)	{

// EXIT POPUP ATIVO/DESATIVO
	$wp_customize->add_setting( 'exit__popup__lampejos__modal_ativo', array(
		'capability' => 'edit_theme_options',
  // 'default' => '_self',
		'sanitize_callback' => 'exit__popup__lampejos__modal__ativado__sanitize__radio',
	) );

	$wp_customize->add_control( 'exit__popup__lampejos__modal_ativo', array(
		'type' => 'radio',
	  'section' => 'exit__popup__lampejos__modal', // Add a default or your own section
	  'label' => 'Exit Popup Ativo?',
	  'description' => 'Selecione se o Exit Popup está ativo ou desativado.',
	  'choices' => array(
	  	'sim' => 'Sim',
	  	'nao' => 'Não',

	  ),
	) );

	function exit__popup__lampejos__modal__ativado__sanitize__radio( $input, $setting ) {
	  // Ensure input is a slug.
		$input = sanitize_key( $input );

	  // Get list of choices from the control associated with the setting.
		$choices = $setting->manager->get_control( $setting->id )->choices;

	  // If the input is a valid key, return it; otherwise, return the default.
		return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
	}

	$wp_customize->add_section( 'exit__popup__lampejos__modal' , array(
		'title'       => __( 'Exit Popup', 'primicia' ),
		'priority'    => 100,
		'description' => 'Configurações do Exit Popup. Personalize de acordo com sua preferência. Não é apresentado em dispositivos móveis.',
	) );



// Imagem
	$wp_customize->add_setting( 'exit_popup_imagem', array(
		'sanitize_callback' => 'esc_url_raw',
	) );

	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize, 'exit_popup_imagem', array(
				'label'    => __( 'Imagem', 'primicia' ),
				'description' => 'Inclua a imagem no tamanho configurado abaixo.',
				'section'  => 'exit__popup__lampejos__modal',
				'settings' => 'exit_popup_imagem',
			)
		)
	);

// Link
	$wp_customize->add_setting('exit__popup__lampejos__modal__url', array(
		'default'     => 'http://',
	));
	
	$wp_customize->add_control('exit__popup__lampejos__modal__url', array(
		'label'  		=> 'Insira o link do modal.',
		'section' 	=> 'exit__popup__lampejos__modal',
		'type'    		=> 'input', // tipo de campo
	));



// TARGET do link
	$wp_customize->add_setting( 'exit__popup__lampejos__modal__url__target', array(
		'capability' => 'edit_theme_options',
  // 'default' => '_self',
		'sanitize_callback' => 'exit__popup__lampejos__modal__url__target__sanitize__radio',
	) );

	$wp_customize->add_control( 'exit__popup__lampejos__modal__url__target', array(
		'type' => 'radio',
	  'section' => 'exit__popup__lampejos__modal', // Add a default or your own section
	  'label' => 'Link em nova aba/janela?',
	  'description' => 'Selecione se o link deverá abrir em uma noja aba/janela.',
	  'choices' => array(
	  	'_self' => 'Não',
	  	'_blank' => 'Sim',
	  ),
	) );

	function exit__popup__lampejos__modal__url__target__sanitize__radio( $input, $setting ) {

	  // Ensure input is a slug.
		$input = sanitize_key( $input );

	  // Get list of choices from the control associated with the setting.
		$choices = $setting->manager->get_control( $setting->id )->choices;

	  // If the input is a valid key, return it; otherwise, return the default.
		return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
	}


// RADIO Página inicial || Todo o site
	$wp_customize->add_setting( 'exit__popup__lampejos__modal__exibicao', array(
		'capability' => 'edit_theme_options',
  'default' => 0,
		'sanitize_callback' => 'exit__popup__lampejos__modal__exibicao__sanitize__radio',
	) );

	$wp_customize->add_control( 'exit__popup__lampejos__modal__exibicao', array(
		'type' => 'radio',
	  'section' => 'exit__popup__lampejos__modal', // Add a default or your own section
	  'label' => 'Selecione o local de exibição',
	  'description' => '',
	  'choices' => array(
	  	0 => 'Página Inicial',
	  	1 => 'Todo o site',
	  ),
	) );

	function exit__popup__lampejos__modal__exibicao__sanitize__radio( $input, $setting ) {

	  // Ensure input is a slug.
		$input = sanitize_key( $input );

	  // Get list of choices from the control associated with the setting.
		$choices = $setting->manager->get_control( $setting->id )->choices;

	  // If the input is a valid key, return it; otherwise, return the default.
		return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
	}


	// Configurações do modal - LARGURA
	$wp_customize->add_setting('exit__popup__lampejos__modal__width', array(
		'default'     => '800',
	));
	
	$wp_customize->add_control('exit__popup__lampejos__modal__width', array(
		'label'  		=> 'Largura',
		'description' => 'Padrão 800px',
		'section' 	=> 'exit__popup__lampejos__modal',
		'type'    	=> 'input', // tipo de campo
	));


	// Configurações do modal - ALTURA
	$wp_customize->add_setting('exit__popup__lampejos__modal__height', array(
		'default'     => '450',
	));
	
	$wp_customize->add_control('exit__popup__lampejos__modal__height', array(
		'label'  		=> 'Altura',
		'description' => 'Padrão 450px',
		'section' 	=> 'exit__popup__lampejos__modal',
		'type'    	=> 'input', // tipo de campo
	));


	// Configurações do modal - CSS
	$wp_customize->add_setting('exit__popup__lampejos__modal__css', array(
		'default'     => '',
	));
	
	$wp_customize->add_control('exit__popup__lampejos__modal__css', array(
		'label'  		=> 'CSS Personalizado',
		// 'description' => 'CSS personalizado',
		'section' 	=> 'exit__popup__lampejos__modal',
		'type'    	=> 'textarea', // tipo de campo
	));
}


function lampejos_footer_exit_popup() {
    include( get_template_directory() . '/dist/exit_popup/_exit_popup.php');
}
add_action( 'wp_footer', 'lampejos_footer_exit_popup');
