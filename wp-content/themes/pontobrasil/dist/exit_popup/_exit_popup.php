<?php

// Verifica se o Exit Popup está ativo/desativo.
$ativo = get_theme_mod( 'exit__popup__lampejos__modal_ativo' );

// Não apresenta em mobile
if ( $ativo === 'sim' && !wp_is_mobile() ) :

// CSS personalizado
	$css = get_theme_mod('exit__popup__lampejos__modal__css');

// Popup Tips
	$banner__imagem = get_theme_mod( 'exit_popup_imagem', $default );
	// $banner__imagem = str_replace( 'http://', 'https://', $banner__imagem ); // Altera http para httpS

	$link = get_theme_mod( 'exit__popup__lampejos__modal__url', $default );
	$link_target =  get_theme_mod( 'exit__popup__lampejos__modal__url__target', $default );

	$local_exibicao = get_theme_mod('exit__popup__lampejos__modal__exibicao', $default);

	$modal_largura = get_theme_mod('exit__popup__lampejos__modal__width', $default);
	$modal_altura = get_theme_mod('exit__popup__lampejos__modal__height', $default);


	?>

	<script type="text/javascript">
		$(document).ready(function(){
			$.stickToMe({
				layer: '#stickLayer',
				interval: 180000, <?php /*Intervalo de tempo entre ativar popups*/ ?>
				cookie : true,
				bgclickclose : true,
				escclose : true,
				onleave : function (e) {},
				disableleftscroll : true  // chrome disable  		
			});
		});
	</script>

	<?php

	// Verifica se tem CSS personalizado
	if (!empty($css)) :
		echo "<style>";
		echo $css;

		echo "#stickLayer { height:" ;
		echo $modal_altura . "px;" ;
		echo"width:" ;
		echo $modal_largura . "px;" ;
		
		echo "</style>";
	 endif; // Fecha CSS



	 if ($local_exibicao == 0) : 
	 	if (is_front_page()) :?>

	 	<div id="stickLayer" style="display:none;" class="stick_popup">
	 	<div class="stick_close" onclick="$.stick_close()">X</div>
	 	<a href="<?php echo $link; ?>" target="<?php echo $link_target;?>">
	 		<div class="stick_content">
	 			<img src="<?php echo $banner__imagem; ?>" alt="banner">
	 		</div>
	 	</a>
	 </div>
	 	
	 <?php endif;
	 else: ?>
	 
	 <div id="stickLayer" style="display:none;" class="stick_popup">
	 	<div class="stick_close" onclick="$.stick_close()">X</div>
	 	<a href="<?php echo $link; ?>" target="<?php echo $link_target;?>">
	 		<div class="stick_content">
	 			<img src="<?php echo $banner__imagem; ?>" alt="banner">
	 		</div>
	 	</a>
	 </div>
	 
	<?php endif;	// local_exibicao

endif; // wp_is_mobile
