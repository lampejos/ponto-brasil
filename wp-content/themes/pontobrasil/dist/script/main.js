$(function(){
	
	/*------------------- Mascaras ------------------------*/

	//$('input[type="tel"]').mask('(99) 999999999');
	$('.telmsk').mask('(99) 9999-9999');
	$('.celmsk').mask('(99) 99999-9999');

	// Outros tipos de Mascaras
	// $('.time').mask('00:00:00');
	// $('.date_time').mask('00/00/0000 00:00:00');
	// $('.cep').mask('00000-000');
	// $('.phone').mask('0000-0000');
	// $('.phone_with_ddd').mask('(00) 0000-0000');
	// $('.phone_us').mask('(000) 000-0000');
	// $('.mixed').mask('AAA 000-S0S');
	// $('.cpf').mask('000.000.000-00', {reverse: true});
	// $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
	// $('.money').mask('000.000.000.000.000,00', {reverse: true});
	// $('.money2').mask("#.##0,00", {reverse: true});

	/*------------------- Fim Mascara --------------------*/


   /*---------- Click com scroll suave pela página  --------*/

	$(document).on('click', 'a[href*="#"]', function () {

		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');

			if (target.length) {
				$('html,body').animate({
					scrollTop: (target.offset().top - 20)
				}, 800);

				return false;
			}
		}
	});

	/*---------- Click com scroll suave na página  --------*/

	window_height = $(window).height();
	body_height = $('body').height();
	footer_height = $('footer').height();
	header_height = $('header').height();
	full_height = window_height - header_height;

	// Div full screen
	$('.home__banner .slick-slider').css('height', full_height);
	

	// if ($(window).width() < 1024){
	// 	$('.home__banner .slick-slider .slick-list .slick-track .slider').css('height', full_height);
	// }
	

	/*----------------- Se a altura da div é menor que o tamanho da tela, 
	considerar o tamanho da tela como altura---------------------*/ 
	
	if(body_height < window_height){
		$('main').height(full_height - footer_height);
		$('body').css('overflow-y', 'hidden');
	}else{
		$('body').css('overflow-y', 'scroll');
		$('main').height('auto');
	}

	if ($(window).width() <= 1024) {
		$('.block-tour').fadeIn(1000);

		$('.block-tour').on('click', function(){
			$('.block-tour').fadeOut(1000);
		});
	};

	if ($(window).width() <= 767) {
		$('.single_planos_text').insertBefore('.single_planos_contentform');
	}

});