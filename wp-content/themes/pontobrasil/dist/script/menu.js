$(function(){

	 $('.menu__responsivo').click(function(){
		$(".menu-top").toggleClass("menu-top-click");
		$(".menu-middle").toggleClass("menu-middle-click");
		$(".menu-bottom").toggleClass("menu-bottom-click");
		$(this).toggleClass('menu-active');

		if($(this).hasClass('menu-active')){
			$('body').css({ 'overflow': 'hidden' });
			$('.header__content-menu').slideDown(500).css({ 'max-height': $(window).height() - $('.header').outerHeight() });
		} else{
			$('body').css({ 'overflow': 'auto' });
			$('.header__content-menu').slideUp(500);
		}
    });


	$(".menu-item-has-children").prepend('<span class="open-submenu"></span>');

    $('.menu-item-has-children').on('click', 'span', function() {

        $(this).toggleClass('open-more');

        if($(this).hasClass('open-more')){
           	$(this).next().next().slideDown(300);
        } else {
            $(this).next().next().slideUp(300);
        }
    });

});