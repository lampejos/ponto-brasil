$(function(){



	var header = $(".header"); 

	var blocoBug = $('.bloco-bug');



	if ($(window).width() >=940) {

		$(window).scroll(function() {

			var scroll = $(window).scrollTop();
			

			if (scroll > 1) {

			    header.addClass("header-fixed");
			    blocoBug.removeClass('fixed').addClass('show');
			    $('.header__bloco__contato').hide();			   
			} else {
				$('.header__bloco__contato').fadeIn();
			    header.removeClass("header-fixed");
			    blocoBug.addClass('hide').removeClass('show');

			}

		});

	} else{

		header.addClass("header-fixed");

	    blocoBug.removeClass('hide').addClass('show');

	}

	

});