



<div class="carrossel">

	<?php $the_query = new WP_Query( array( 'post_type' => 'slider',  'posts_per_page' => 5  ) );  ?>



	<?php 

	if ( $the_query->have_posts() ) :

		while ( $the_query->have_posts() ) : $the_query->the_post(); 

			?>

			<?php 

			$link = _wpt_slider_pagina_get_meta( '_wpt_slider_pagina_link' );
			$target = _wpt_slider_pagina_get_meta( '_wpt_slider_pagina_target' );
			$linha_fina = _wpt_slider_pagina_get_meta( '_wpt_slider_pagina_linha_fina' );
			$button_text = _wpt_slider_pagina_get_meta( '_wpt_slider_pagina_buttontext' );
			?>


			<div class="slider 1 home__banner flex align-center just-center direction-col" style="background: url('<?php the_post_thumbnail_url();?>') no-repeat center center;  background-size: cover;">
				<div class="row just-center mg-b-65">
					<div class="col-lg-8 col-md-8 col-sm-7">
						<h1 class="yellow"><?php echo get_the_title(); ?></h1>
					</div>
				</div>

				<?php if (!empty($linha_fina)){ ?>
				<div class="row just-center mg-b-65">
					<div class="col-lg-7 col-md-7 col-sm-6">
						<p><?php echo $linha_fina; ?></p>
					</div>
				</div>					
				<?php } ?>

				<?php if (!empty($link)) { ?>
				<div class="row">
					<div class="col-lg-12">
						<a href="<?php echo $link; ?>" <?php if (!empty($target)){ ?>target="_blank"<?php }  ?> >
							<div class="button-md yellow br_yellow"><?php echo $button_text; ?></div>	
						</a>
					</div>
				</div>

				<?php } ?>
		
			</div>			
				

	<?php endwhile;	endif; wp_reset_query();?>

</div>



<script type="text/javascript">

	$(document).ready(function(){

		$('.carrossel').slick({

			infinite: true,

			autoplay: true,

			autoplaySpeed: 4000,

			dots: false,

			arrows: false

		});

	});

</script>







