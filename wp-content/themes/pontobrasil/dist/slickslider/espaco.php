<div id="espaco" class="box home__espacos__container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bg_yellow home__espacos__text">
			<a href="<?php echo the_permalink(132); ?>" class="home__espacos__text_link">
				<h2><?php the_field('espaco_titulo', 'option'); ?></h2>
			</a>
			<?php the_field('espaco_texto', 'option'); ?>
			<a href="<?php echo the_permalink(348); ?>" class="btn__link">
				<div class="button-md blue br_blue flex align-center just-center">Faça o tour virtual</div>
			</a>
			<a href="<?php echo the_permalink(308); ?>" class="btn__link">
				<div class="button-md blue br_blue flex align-center just-center">Galeria de fotos</div>
			</a>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-xs-offset-3 home__espacos__slider espaco__slider">

			<?php 
			if( have_rows('espaco_slider', 'option') ): 
				while ( have_rows('espaco_slider', 'option') ) : the_row(); 
			?>		
					<img src="<?php the_sub_field('espaco_slider_imagem'); ?>" class="img-auto">

			<?php 
				endwhile; 
			endif; 
			?>   

		</div>

	</div>					
</div>

<script type="text/javascript">

  $(document).ready(function(){

    
    $('.espaco__slider').slick({
      dots: true,
      arrows: false,
      infinite: true,
      speed: 500,
      autoplay: true,
      autoplaySpeed: 4000,
      slidesToShow: 1,
      slidesToScroll: 1,
    });
    
  });


</script>