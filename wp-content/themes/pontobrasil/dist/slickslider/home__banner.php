<div class="carrossel">

	<?php $the_query = new WP_Query( array( 'post_type' => 'slider',  'posts_per_page' => 5  ) );  ?>

	<?php 

	if ( $the_query->have_posts() ) :

		while ( $the_query->have_posts() ) : $the_query->the_post(); 

			?>

			<?php 

			$link = _wpt_slider_pagina_get_meta( '_wpt_slider_pagina_link' );
			$target = _wpt_slider_pagina_get_meta( '_wpt_slider_pagina_target' );
			$linha_fina = _wpt_slider_pagina_get_meta( '_wpt_slider_pagina_linha_fina' );
			$button_text = _wpt_slider_pagina_get_meta( '_wpt_slider_pagina_buttontext' );
			?>

			<div class="slider flex align-center just-center direction-col" style="background: url('<?php the_post_thumbnail_url();?>') no-repeat center center;  background-size: cover;">
				<div class="bg_overlay flex direction-col just-center align-center width-full height-full">
					<div class="row just-center mg-b_65">
						<div class="col-xs-12 col-sm-10 col-md-8 col-lg-8">
							<h1 class="yellow txt-center"><?php echo get_the_title(); ?></h1>
						</div>
					</div>

					<?php if (!empty($linha_fina)){ ?>
					<div class="row just-center mg-b_65">
						<div class="col-xs-12 col-sm-10 col-md-7 col-lg-7">
							<p class="white txt-center txt-uppercase mg-0"><?php echo $linha_fina; ?></p>
						</div>
					</div>					
					<?php } ?>

					<?php if (!empty($link)) { ?>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<a href="<?php echo $link; ?>" <?php if (!empty($target)){ ?>target="_blank"<?php }  ?> >
								<div class="button-lg yellow br_yellow"><?php echo $button_text; ?></div>	
							</a>
						</div>
					</div>

					<?php } ?>
				</div>		
			</div>			
				
	<?php endwhile;	endif; wp_reset_query();?>

</div>

<script type="text/javascript">

	$(document).ready(function(){

		$('.carrossel').slick({

			infinite: true,

			autoplay: true,

			autoplaySpeed: 5000,

			dots: false,

			arrows: false

		});

	});

</script>







