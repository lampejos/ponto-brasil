<div class="col-lg-12 pd-0 facilidades__slider">
	<img src="<?php echo get_template_directory_uri(); ?>/dist/images/facilidades__slider1.jpg" class="img-auto" alt="">
	<img src="<?php echo get_template_directory_uri(); ?>/dist/images/facilidades__slider2.jpg" class="img-auto" alt="">
	<img src="<?php echo get_template_directory_uri(); ?>/dist/images/facilidades__slider3.jpg" class="img-auto" alt="">
	<img src="<?php echo get_template_directory_uri(); ?>/dist/images/facilidades__slider4.jpg" class="img-auto" alt="">
	<img src="<?php echo get_template_directory_uri(); ?>/dist/images/facilidades__slider5.jpg" class="img-auto" alt="">
</div>

<script type="text/javascript">

  $(document).ready(function(){

    
    $('.facilidades__slider').slick({
      dots: false,
      arrows: true,
      infinite: false,
      speed: 300,
      slidesToShow: 5,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            initialSlide: 3,           
            slidesToShow: 3,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 768,
          settings: {
            initialSlide: 2,
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 425,
          settings: {
            initialSlide: 2,
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },      
      ]
    });
    
  });


</script>