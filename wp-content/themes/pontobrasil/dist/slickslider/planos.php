<div id="planos" class="bg_lightblue">
  <div class="content-center">
    <div class="box home__planos__container b-shadow">

      <div class="row just-center">
        <div class="col-12"><h2 class="txt-center">Nossos Planos</h2></div>          
      </div>

        <div class="row planos__slider">

            <?php 
              $args = array(
                'numberposts' => -1,
                'post_type'   => 'planos_post'
              );
               
              $planos = get_posts( $args );

              $planos = get_field('opcao_planos_slider', 'option');

              if( $planos ):

                foreach($planos as $plano):
                  setup_postdata($planos);
                  //var_dump($plano);

                  $post_id = $plano->ID;
              ?>      

                  <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2 home__planos__box flex direction-col align-center">
                    <p class="blue flex align-center"><?php echo get_the_title($post_id); ?></p>
                    <span class="home__planos__box__text">a partir de</span>
                    <div class="home__planos__box__price flex align-start">      
                      <span class="green"><?php the_field('plano_preco', $post_id); ?>,</span>
                      <span class="green"><?php the_field('plano_preco_decimal', $post_id); ?></span>
                    </div>
                    <span class="home__planos__box__text plano_itens flex align-center"><?php the_field('plano_itens', $post_id); ?></span>
                    <a href="<?php echo get_permalink($post_id); ?>" title="Consultar o plano de <?php echo get_the_title($post_id); ?>">
                      <div class="button-sm green br_green flex align-center just-center">Consulte</div>
                    </a>
                  </div> 

                <?php endforeach; ?>

              <?php wp_reset_postdata(); ?>
              <?php endif; ?>
        </div>

    </div>
  </div>
</div>

<script type="text/javascript">

  $(document).ready(function(){

    
    $('.planos__slider').slick({
      dots: false,
      arrows: true,
      infinite: false,
      speed: 300,
      slidesToShow: 6,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4,
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 618,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 470,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });
    
  });


</script>