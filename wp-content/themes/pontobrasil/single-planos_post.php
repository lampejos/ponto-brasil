<?php get_header(); ?> 

	<main class="main" role="main">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<div class="container-fluid">

					<div class="row">						
					    
				    	<div class="content-center content-single content-no-thumb">		        
				        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pd-0">
				        		
				        		<div class="single_planos_bg" style="background: url('<?php the_post_thumbnail_url(); ?>')  no-repeat center center;  background-size: cover; ">

				        			<?php if ( has_post_thumbnail() ) : ?>

						        			<div class="bg_overlay flex direction-col just-center width-full height-full">
						        				<h1 class="yellow txt-center"><?php the_title(); ?></h1>
						        				<p class="white txt-uppercase txt-center"><?php the_field('plano_linha_fina'); ?></p>
						        			</div>


									 <?php else : ?>

									 		<div class="flex direction-col just-center width-full height-full">	
							        			<h1 class="yellow txt-center"><?php the_title(); ?></h1>
							        			<p class="grey txt-uppercase txt-center"><?php the_field('plano_linha_fina'); ?></p>
						        			</div>
				        			       				        			  
				        			<?php endif; ?>		

				        		</div>		

				        		<div class="row">
				        			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 single_planos_contentform">
				        				<div class="single_planos_form bg_yellow">
					        				<h2>Solicite</h2>
					        				<p class="blue txt-uppercase">receba no seu email nossa tabela completa de serviços e valores</p>
					        				<?php echo do_shortcode('[contact-form-7 id="140" title="Solicitar Serviços"]'); ?>
				        				</div>		
				        			</div>


				        			<div class="col-xs-offset-1 col-xs-12 col-sm-12 col-md-7 col-lg-7 single_planos_text">
										<h3>Características do Plano</h3>
				        				<?php the_content(); ?>

				        				<h3>Preço</h3>
				        				<p><?php the_field('plano_preco', $post_id); ?>,<?php the_field('plano_preco_decimal', $post_id); ?></p>

				        				<h3>Sobre o Coworking</h3>
				        				<p><?php the_field('quem_somos_resumo', 132); ?></p>
				        			</div>
				        		</div>

				        	</div>					        
					    </div>
					</div>

				</div>			
			
			</div>
			
		<?php endwhile; endif; ?>

		<script type="text/javascript">
			$(".single_planos_form").stick_in_parent({
				offset_top: 80
			});
		</script>
	</main>

<?php get_footer(); ?>