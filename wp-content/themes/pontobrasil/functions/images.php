<?php

/** Ativa as thumbnails */
add_theme_support('post-thumbnails');

/** Redimencionamento de imagens */

/* Default WordPress */
add_image_size('thumbnail', 		363,  233,	array('center', 'center'));
add_image_size('medium',    		386,  316,	true); 
add_image_size('medium_large',	      0,    0,	true);
add_image_size('large',     		1113, 405,	true);
add_image_size('slider',     		 846, 656,	true);
