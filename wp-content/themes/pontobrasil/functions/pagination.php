<?php

// Link: https://artisansweb.net/load-wordpress-post-ajax/



/* -------------------- Pagination Blog ----------------------*/

function load_posts_by_ajax_callback() {
    check_ajax_referer('load_more_posts', 'security');
    $paged = $_POST['page'];
    
    $args_recent = array( 'numberposts' => '1' );
    $recent_posts = wp_get_recent_posts( $args_recent );
    $id_arr = [];
    foreach( $recent_posts as $recent ){
        $id_arr[] = $recent_posts[0]['ID'];
    }   

    $args = array(
        'post_type'         => 'post',
        'posts_per_page'    => 6,
        'paged'             => $paged,
        'post__not_in'       => $id_arr
    );

    $more_posts = new WP_Query( $args );
    if ( $more_posts->have_posts() ) :
        ?>
        <?php while ( $more_posts->have_posts() ) : $more_posts->the_post();
            echo '<a href="'.get_permalink().'" class="col-xs-12 col-sm-6 col-md-4 col-lg-4 flex direction-col just-center pd-b_60 blog__thumb__single">';
                 

                echo '<div class="zoom_img">';                                              
                    echo '<img src="' . get_the_post_thumbnail_url(get_the_ID(), 'thumbnail') . '">';
                echo '</div>';

                echo '<div class="flex">';
                    foreach((get_the_category()) as $category) {                                        
                        echo '<span class="post-categories">' .$category->cat_name. '</span>'; 
                    }
                echo '</div>';                                                  

                echo '<h3>';
                    the_title();
                echo '</h3>';
                
                echo '<div class="flex align-end">';
                    echo '<div class="button-sm blue br_blue flex align-center just-center">Leia Mais</div>';
                echo '</div>';

            echo '</a>';
        endwhile; ?>

        <?php else: ?>
            <div class="width-full txt-center">
                <p>Todos os posts já foram exibidos!</p>
            </div>
            <script>                
                 $('#more_posts').hide();
            </script>

    <?php endif;
 
    wp_die();
}

add_action('wp_ajax_load_posts_by_ajax', 'load_posts_by_ajax_callback');
add_action('wp_ajax_nopriv_load_posts_by_ajax', 'load_posts_by_ajax_callback');




function load_fotos_callback() {
    if( have_rows('option_galeria_imagens', 'option') ):
        while ( have_rows('option_galeria_imagens', 'option') ) : the_row();

            echo '<a data-fancybox="gallery" href="' . get_sub_field('option_galeria_imagens_img') . '" data-caption="' . get_sub_field('option_galeria_imagens_titulo') .'" class="col-xs-12 col-sm-12 col-md-3 col-lg-4 flex direction-col just-center pd-b_60 blog__thumb__single img-effect">';
                echo '<div class="zoom_img">';                                              
                echo '<img src="' . get_sub_field('option_galeria_imagens_img') . '">';
                echo '<div class="img-hover"><span>' . get_sub_field('option_galeria_imagens_titulo') . '</span></div>';
                echo '</div>';
            echo '</a>';    
        endwhile;
    endif;
 
    wp_die();
}

add_action('wp_ajax_load_posts_by_ajax', 'load_fotos_callback');
add_action('wp_ajax_nopriv_load_posts_by_ajax', 'load_fotos_callback');

