<?php

	/*------------------ Criação de Cpt---------------------*/
	
	// Geral

	$textdomain = 'planos';


	// Post Type

	function planos_post() {
		$labels = array(
			'name'                  => _x( 'Planos', 'Post Type General Name', $textdomain ), // Nome que aparece na criação do menu
			'singular_name'         => _x( 'Plano', 'Post Type Singular Name', $textdomain ), 
			'menu_name'             => __( 'Planos', $textdomain ), //Nome que aparece na Aba do Wordpress					
			'name_admin_bar'        => __( 'Planos', $textdomain ), 							
			'archives'              => __( 'Todas os Planos', $textdomain ),					
			'attributes'            => __( 'Atributos', $textdomain ),
			'parent_item_colon'     => __( 'Item pai:', $textdomain ),
			'all_items'             => __( 'Todos os itens', $textdomain ),
			'add_new_item'          => __( 'Adicionar', $textdomain ),
			'add_new'               => __( 'Adicionar', $textdomain ),
			'new_item'              => __( 'Novo', $textdomain ),
			'edit_item'             => __( 'Editar', $textdomain ),
			'update_item'           => __( 'Atualizar', $textdomain ),
			'view_item'             => __( 'Ver', $textdomain ),
			'view_items'            => __( 'Ver itens', $textdomain ),
			'search_items'          => __( 'Procurar', $textdomain ),
			'not_found'             => __( 'Não encontrado', $textdomain ),
			'not_found_in_trash'    => __( 'Não encontrado na lixeira', $textdomain ),
			'featured_image'        => __( 'Imagem destacada', $textdomain ),
			'set_featured_image'    => __( 'Definir Imagem Destacada', $textdomain ),
			'remove_featured_image' => __( 'Remover Imagem Destacada', $textdomain ),
			'use_featured_image'    => __( 'Usar como imagem destacada', $textdomain ),
			'insert_into_item'      => __( 'Inserir no item', $textdomain ),
			'uploaded_to_this_item' => __( 'Carregar item', $textdomain ),
			'items_list'            => __( 'Lista de iten', $textdomain ),
			'items_list_navigation' => __( 'Navegar lista de itens', $textdomain ),
			'filter_items_list'     => __( 'Lista de itens de filtro', $textdomain ),
		);

		$rewrite = array(
			'slug'                  => 'planos', //Como será mostrado na url esse cpt
			'with_front'            => true,
			'pages'                 => true,
			'feeds'                 => true,
		);

		$args = array(
			'label'                 => __( 'Planos', $textdomain ),
			'description'           => __( 'Planos', $textdomain ), // Descrição desse cpt
			'labels'                => $labels,
			'supports'              => array('title', 'editor', 'thumbnail', 'revisions'),
			'taxonomies'            => array(), // Inserir aqui o nome da taxonomia criada ou utilizar 'category, tag' que é default do Wordpress
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true, // Mostrar esse cpt na criação de menu
			'menu_position'         => 3, // Posição no painel do wordpress
			'menu_icon'             => 'dashicons-cart', //Icone da aba no painel. (Customizar: https://developer.wordpress.org/resource/dashicons/#thumbs-down)
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true, 
			'has_archive'           => false, // Ativar/Desativar a página que traz todos os posts (Archive Page)		
			'exclude_from_search'   => false, // Trazer os posts como resultado de busca
			'publicly_queryable'    => true, 
			'query_var'             => 'planos_post', // Inserir aqui o slug do cpt
			'rewrite'               => $rewrite,
			'capability_type'       => 'post',
		);
		register_post_type( 'planos_post', $args );
	}

	add_action( 'init', 'planos_post', 0 );





	// // Taxonomy

	// function clientes_tax() {
	// 	$labels = array(
	// 		'name'                       => _x( 'Empresas', 'Taxonomy General Name', $textdomain ), // Nome que aparece na criação do menu 
	// 		'singular_name'              => _x( 'Empresa', 'Taxonomy Singular Name', $textdomain ),
	// 		'menu_name'                  => __( 'Empresas', $textdomain ), //Nome que aparece na Aba do Wordpress
	// 		'all_items'                  => __( 'Todas as empresas', $textdomain ),
	// 		'parent_item'                => __( 'Item pai', $textdomain ),
	// 		'parent_item_colon'          => __( 'Item Parent', $textdomain ),
	// 		'new_item_name'              => __( 'Nome', $textdomain ),
	// 		'add_new_item'               => __( 'Adicionar novo', $textdomain ),
	// 		'edit_item'                  => __( 'Editar', $textdomain ),
	// 		'update_item'                => __( 'Atualizar', $textdomain ),
	// 		'view_item'                  => __( 'Ver', $textdomain ),
	// 		'separate_items_with_commas' => __( 'Separar itens com vírgulas', $textdomain ),
	// 		'add_or_remove_items'        => __( 'Adicionar/remover itens', $textdomain ),
	// 		'choose_from_most_used'      => __( 'Escolher a mais usada', $textdomain ),
	// 		'popular_items'              => __( 'Itens populares', $textdomain ),
	// 		'search_items'               => __( 'Pesquisar', $textdomain ),
	// 		'not_found'                  => __( 'Não encontrado', $textdomain ),
	// 		'no_terms'                   => __( 'Nenhum item', $textdomain ),
	// 		'items_list'                 => __( 'Lista de itens', $textdomain ),
	// 		'items_list_navigation'      => __( 'Navegação lista de itens', $textdomain ),
	// 	);

	// 	$rewrite = array(
	// 		'slug'                       => 'empresas',
	// 		'with_front'                 => true,
	// 		'hierarchical'               => false,
	// 	);

	// 	$args = array(
	// 		'labels'                     => $labels,
	// 		'hierarchical'               => true,
	// 		'public'                     => true,
	// 		'show_ui'                    => true,
	// 		'show_in_menu'          	 => true,
	// 		'show_admin_column'          => true,
	// 		'show_in_nav_menus'          => true,
	// 		'show_tagcloud'              => true,
	// 		'rewrite'                    => $rewrite,
	// 	);
	// 	register_taxonomy( 'clientes_tax', array('clientes_post'), $args ); // Colocar o slug do post que você criou
	// }

	// add_action( 'init', 'clientes_tax', 0 );





	// // Create Tag

	// function clientes_tag() {
	// 	$labels = array(
	// 		'name'                       => _x( 'Tags Clientes', 'Taxonomy General Name', $textdomain ),
	// 		'singular_name'              => _x( 'Tag', 'Taxonomy Singular Name', $textdomain ),
	// 		'menu_name'                  => __( 'Tags', $textdomain ),
	// 		'all_items'                  => __( 'Todas as tags', $textdomain ),
	// 		'parent_item'                => __( 'Item Pai', $textdomain ),
	// 		'parent_item_colon'          => __( 'Item pai', $textdomain ),
	// 		'new_item_name'              => __( 'Novo nome', $textdomain ),
	// 		'add_new_item'               => __( 'Adicionar', $textdomain ),
	// 		'edit_item'                  => __( 'Editar', $textdomain ),
	// 		'update_item'                => __( 'Atualizar', $textdomain ),
	// 		'view_item'                  => __( 'Ver', $textdomain ),
	// 		'separate_items_with_commas' => __( 'Separar itens com vírgula', $textdomain ),
	// 		'add_or_remove_items'        => __( 'Adicionar ou remover itens', $textdomain ),
	// 		'choose_from_most_used'      => __( 'Escolher mais usados', $textdomain ),
	// 		'popular_items'              => __( 'Itens Populares', $textdomain ),
	// 		'search_items'               => __( 'Pesquisar Itens', $textdomain ),
	// 		'not_found'                  => __( 'Não encontrado', $textdomain ),
	// 		'no_terms'                   => __( 'Nenhum item', $textdomain ),
	// 		'items_list'                 => __( 'Listar Item', $textdomain ),
	// 		'items_list_navigation'      => __( 'Lista de itens', $textdomain ),
	// 	);

	// 	$args = array(
	// 		'labels'                     => $labels,
	// 		'hierarchical'               => false,
	// 		'public'                     => true,
	// 		'show_ui'                    => true,
	// 		'show_admin_column'          => true,
	// 		'show_in_nav_menus'          => true,
	// 		'show_tagcloud'              => true,
	// 		'show_in_rest'               => true,
	// 		'rest_base'                  => 'clientes_tag',
	// 		'rest_controller_class'      => 'clientes_tag',
	// 	);

	// 	register_taxonomy( 'clientes_tag', array( 'clientes_post' ), $args ); // Colocar o slug do post que você criou
	// }

	// add_action( 'init', 'clientes_tag', 0 );

	// 