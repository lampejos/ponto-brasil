<?php 

/** Header */
function header_scripts() {
	if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
		
		// Css Geral
		wp_register_style('style', get_template_directory_uri() . '/dist/style/main.css', array(), '1.0', 'all');
		wp_enqueue_style('style');

		// Css Grid Flex
		wp_register_style('flexgrid', get_template_directory_uri() . '/dist/style/vendor/flexboxgrid.css', array(), '1.0', 'all');
		wp_enqueue_style('flexgrid');

		// Header
		wp_register_style('header', get_template_directory_uri() . '/dist/style/header.css', array(), '1.0', 'all');
		wp_enqueue_style('header');

		wp_register_style('structure', get_template_directory_uri() . '/dist/style/structure.css', array(), '1.0', 'all');
		wp_enqueue_style('structure');

		// jQuery
		wp_register_script('jquery', '//code.jquery.com/jquery-3.2.1.min.js', array(), '3.2.1');
		wp_enqueue_script('jquery');

		// Position Fixed Script
		wp_register_script('stickFixed', get_template_directory_uri() . '/dist/script/vendor/jquery.sticky-kit.min.js', array(), '1.0');
		wp_enqueue_script('stickFixed');

		// Mascara de Inputs
		wp_register_script('fancyboxJS', get_template_directory_uri() . '/dist/script/vendor/jquery.fancybox.min.js', array(), '1.0');
		wp_enqueue_script('fancyboxJS');

		// Css Menu Responsivo
		wp_register_style('fancyboxCss', get_template_directory_uri() . '/dist/style/vendor/jquery.fancybox.min.css', array(), '1.0', 'all');
		wp_enqueue_style('fancyboxCss');
	}
}
add_action('wp_enqueue_scripts', 'header_scripts');



/** Footer */
function footer_scripts() {
	if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

		// Javascript principal
		wp_register_script('main', get_template_directory_uri() . '/dist/script/main.js', array(), '1.0');
		wp_enqueue_script('main');

		// Javascript Header
		wp_register_script('headerJS', get_template_directory_uri() . '/dist/script/header.js', array(), '1.0');
		wp_enqueue_script('headerJS');

		// Css Menu Responsivo
		wp_register_style('menu', get_template_directory_uri() . '/dist/style/menu.css', array(), '1.0', 'all');
		wp_enqueue_style('menu');

		// Javascript Menu Responsivo
		wp_register_script('menuJs', get_template_directory_uri() . '/dist/script/menu.js', array(), '1.0');
		wp_enqueue_script('menuJs');

		// Mascara de Inputs
		wp_register_script('maskInput', get_template_directory_uri() . '/dist/script/vendor/jquery.mask.min.js', array(), '1.0');
		wp_enqueue_script('maskInput');

		// Google Fonts
		wp_register_style('google_Roboto', 'https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet', array(), '1.0', 'all');
		wp_enqueue_style( 'google_Roboto');

		wp_register_style('google_Roboto_Condensed', 'https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet', array(), '1.0', 'all');
		wp_enqueue_style( 'google_Roboto_Condensed');

		wp_register_style('google_Roboto_Slab', 'https://fonts.googleapis.com/css?family=Roboto+Slab:400,700" rel="stylesheet" rel="stylesheet', array(), '1.0', 'all');
		wp_enqueue_style( 'google_Roboto_Slab');

		


		// ex.: Modernizr
		// wp_register_script('plugins', get_template_directory_uri() . '/dist/script/vendor/modernizr.min.js', array(), '2.6.2');
		// wp_enqueue_script('plugins');

	}
}
add_action('wp_footer', 'footer_scripts');






/* -- Remove o número da versões dos arquivos CSS e JS */
/*
add_filter( 'style_loader_src',  'sdt_remove_ver_css_js', 9999, 2 );
add_filter( 'script_loader_src', 'sdt_remove_ver_css_js', 9999, 2 );

function sdt_remove_ver_css_js( $src, $handle ) 
{
    $handles_with_version = [ 'style' ]; // <-- Adjust to your needs!

    if ( strpos( $src, 'ver=' ) && ! in_array( $handle, $handles_with_version, true ) )
        $src = remove_query_arg( 'ver', $src );

    return $src;
}
*/

