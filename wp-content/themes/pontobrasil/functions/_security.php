<?php
	
	
/* Desabilita editor no Dashboard */
define( 'DISALLOW_FILE_EDIT', true );

/**
 * Remove Pingback XMLRPC
 *
 * Remove funções dos comentários e bloqueia o uso por aplicações externas
 */
function Remove_Pingback_Method( $methods ) {
	unset( $methods['pingback.ping'] );
	unset( $methods['pingback.extensions.getPingbacks'] );
	return $methods;
}
add_filter( 'xmlrpc_methods', 'Remove_Pingback_Method' );


// Remove as mensagens de erro de login
add_filter('login_errors', create_function('$a', "return null;") );
