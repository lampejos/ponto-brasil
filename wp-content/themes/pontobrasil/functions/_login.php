<?php

/** Personaliza a tela de login */
function custom_login_logo() {
	echo '
	<style type="text/css">
		.login h1 a {
			background: url("' . get_bloginfo('template_directory') . '/dist/images/wp-admin-logo.png") center no-repeat;
			width: 200px;
			height: 200px;
			margin-bottom: 0;
			display: block;
			background-size: contain;
		}
	</style>';
}
add_action('login_head', 'custom_login_logo');

function custom_login_credits() {
	echo '<p style="text-align: center"><a href="https://lampejos.com.br" style="color: #4da28f; text-decoration: none;">Desenvolvido por Lampejos.com.br</a></p>';
}
add_action('login_footer', 'custom_login_credits');