<?php 

/** Funções exclusivas do tema */


/* ------------------------------------------------

EXCERPT - Alterando o core

--------------------------------------------------*/

/* Diminui o número de palavras no resumo */
function lamps_excerpt_limite( $length ) {
	return 15;
}
add_filter( 'excerpt_length', 'lamps_excerpt_limite', 999 );

/* Remover o caracter [...] no final do resumo */
function lamps_excerpt_remove( $more ) {
    return '... [+]';
}
add_filter( 'excerpt_more', 'lamps_excerpt_remove' );





/* ------------------------------------------------

REMOVE a versão do WordPress dos arquivos do FEED

--------------------------------------------------*/

add_filter( 'get_the_generator_html', '__return_false' );
add_filter( 'get_the_generator_xhtml', '__return_false' );
add_filter( 'get_the_generator_atom', '__return_false' );
add_filter( 'get_the_generator_rss2', '__return_false' );
add_filter( 'get_the_generator_rdf', '__return_false' );
add_filter( 'get_the_generator_comment', '__return_false' );
add_filter( 'get_the_generator_export', '__return_false' );






/* ------------------------------------------------

REMOVE o author e <dv:creator> do Feed e qualquer outro local que apareça

--------------------------------------------------*/

function lampejos_fake_author( $display_name ) {

    // Author será o nome do site
		$author = get_bloginfo( 'name' );

    if ( is_feed() ) {
        return $author;
    }

    return $author;
}

 add_filter( 'the_author', 'lampejos_fake_author', PHP_INT_MAX, 1 );