<?php

/** Registra menus */
function register_menu() {
	register_nav_menu('header-menu',  ('Header Menu')  );
	register_nav_menu('footer-menu',  ('Footer Menu')  );
	register_nav_menu('sidebar-menu', ('Sidebar Menu') );
	register_nav_menu('extra-menu',   ('Extra Menu')   );
}
add_action('init', 'register_menu');