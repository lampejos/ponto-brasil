<?php

	if( function_exists('acf_add_options_page') ) {
		
		acf_add_options_page(array(
			'page_title' 	=> 'Customizações',
			'menu_title'	=> 'Customizações',
			'menu_slug' 	=> 'theme-general-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> true
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Geral',
			'menu_title'	=> 'Geral',
			'parent_slug'	=> 'theme-general-settings',
		));	
		
		
		acf_add_options_sub_page(array(
			'page_title' 	=> 'Serviços',
			'menu_title'	=> 'Serviços',
			'parent_slug'	=> 'theme-general-settings',
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Planos',
			'menu_title'	=> 'Planos',
			'parent_slug'	=> 'theme-general-settings',
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Espaço',
			'menu_title'	=> 'Espaço',
			'parent_slug'	=> 'theme-general-settings',
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Facilidades',
			'menu_title'	=> 'Facilidades',
			'parent_slug'	=> 'theme-general-settings',
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Galeria',
			'menu_title'	=> 'Galeria',
			'parent_slug'	=> 'theme-general-settings',
		));
				
	}
	