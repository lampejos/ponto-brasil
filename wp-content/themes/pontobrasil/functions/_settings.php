<?php

/** Título automático */
add_theme_support('title-tag');


/** RSS feed automático */
add_theme_support('automatic-feed-links');


/** Remove a barra de admin do front-end */
add_filter("show_admin_bar" , create_function('$a', "return false;"));


/** 
 * Aprimora as classes adicionas no <body>
 * 
 * Remove a classe '.blog' da página inicial
 * Adiciona o '.nome-do-post' nas singles
 */
function add_slug_to_body_class( $classes ) {
	global $post;
	if (is_home() or is_front_page()) {
		$key = array_search('blog', $classes);
		if ($key > -1) {
			unset($classes[$key]);
		}
	} elseif ( is_page() or is_singular() ) {
		$classes[] = sanitize_html_class($post->post_name);
	}
	return $classes;
}
add_filter('body_class', 'add_slug_to_body_class');


/** Tamanho máximo dos embeds */
if ( !isset($content_width) ) {
	// Tamanho do container do conteudo
	$content_width = 960;
}


/** Altera mensagem no rodapé no painel */
function altera_footer_admin () {
	$lamp_site_url = get_bloginfo("url");
	$lamp_site_name = get_bloginfo("name");
	echo 'Desenvolvido por <a href="http://lampejos.com.br/?&utm_source=' . $lamp_site_url . '&utm_medium=link&utm_content=WordpressFooter&utm_campaign=' . $lamp_site_name . '" target="_blank">Lampejos</a>.';
}

add_filter('admin_footer_text', 'altera_footer_admin');



/** Ordena resultados de busca por data */
function my_search_query( $query ) {
	// not an admin page and is the main query
	if ( !is_admin() && $query->is_main_query() ) {
		if ( is_search() ) {
			$query->set( 'orderby', 'date' );
		}
	}
}
add_action( 'pre_get_posts', 'my_search_query' );
