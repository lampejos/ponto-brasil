<?php 
	/* Template Name: Galeria*/
	get_header(); 
?> 

	<main class="main" role="main">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<div class="container-fluid">

					<div class="row">						
					    
				    	<div class="content-center content-page">					        				    					  
				        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				        		<h1 class="txt-center"><?php the_title(); ?></h1>

				        		<?php the_content(); ?>

				        		<div class="row blog__thumb__post align-start">
				        	
				        			<?php
										if( have_rows('option_galeria_imagens', 'option') ):
										    while ( have_rows('option_galeria_imagens', 'option') ) : the_row();
										    	$image = get_sub_field('option_galeria_imagens_img'); 
												$slider = 'slider'; 
												$medium = 'medium'; 
												$thumb = $image['sizes'][ $medium ];
												$full = $image['sizes'][ $slider ];

										    	echo '<a data-fancybox="gallery" href="' . $full . '" data-caption="' . get_sub_field('option_galeria_imagens_titulo') .'" class="col-xs-12 col-sm-6 col-md-4 col-lg-4 flex direction-col just-center pd-b_60 blog__thumb__single img-effect">';
												  	echo '<div class="zoom_img">';     
												  		echo '<img src="' . $thumb . '">';	  	
											  			echo '<div class="img-hover"><span>' . get_sub_field('option_galeria_imagens_titulo') . '</span></div>';
											  		echo '</div>';
												echo '</a>';	
										    endwhile;
										endif;
									?>  
				        		</div>

				        	</div>					        
					    </div>

					</div>

					
					<?php  include('dist/slickslider/planos.php');  ?>

				</div>			
			
			</div>
			
		<?php endwhile; endif; ?>

		<script type="text/javascript">
			$(function(){
				$('[data-fancybox]').fancybox({
					toolbar  : 'auto',
					infobar	 : false,
					buttons: [
					    "close"
					]
				})
			});
		</script>
	</main>

<?php get_footer(); ?>