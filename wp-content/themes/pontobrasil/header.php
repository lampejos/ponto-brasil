<!doctype html>
<!-- Projeto desenvolvido por lampejos.com.br -->
<!--[if IE 6]><html <?php language_attributes(); ?> class="ie6"><![endif]-->
<!--[if IE 7]><html <?php language_attributes(); ?> class="ie7"><![endif]-->
<!--[if IE 8]><html <?php language_attributes(); ?> class="ie8"><![endif]-->
<!--[if gte IE 8]><html <?php language_attributes(); ?> class="ie9"><![endif]-->

<!--[if !IE]><!-->
<html <?php language_attributes(); ?> itemscope itemtype="http://schema.org/WebPage">
<!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0" />

	<link rel="dns-prefetch" href="//fonts.googleapis.com">
	<link rel="dns-prefetch" href="//google-analytics.com">	

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<?php wp_head(); ?>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-K4K5WFN');</script>
	<!-- End Google Tag Manager --> 
</head>

<body <?php body_class(); ?>>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K4K5WFN"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) --> 

	<header class="header">

		<div class="content-center height-full">
			
			<div class="row align-center height-full header__align">
			    <div class="col-xs-6 col-sm-6 col-md-2 col-lg-3 header__content__logo">
			        <div class="box">
			        	<a href="<?php echo home_url(); ?>">
			        		<img src="<?php echo get_template_directory_uri(); ?>/dist/images/logo-pontobrasil.png" alt="Ponto Brasil">
			        	</a>
			        </div>
			    </div>

			    <div class="col-xs-6 col-sm-6 col-md-10 col-lg-9 header__content-menu">
					<div class="box">				
						<nav>
							<?php 
								// Não trocar a classe 'header__nav__menu' pois o menu hamburguer depende dela.
								wp_nav_menu( array( 'theme_location' => 'header-menu', 'menu_class' => 'header__nav__menu' ) ); 
							?>
						</nav>
					</div>			
			    </div>

			    <div class="col-xs-2">
			        <div class="box">
						<div class="menu__responsivo">
							<span class="menu-global menu-top"></span>
							<span class="menu-global menu-middle"></span>
							<span class="menu-global menu-bottom"></span>
						</div>
			        </div>
			    </div>
			</div>	
			
		</div>

    


	</header>

	<div class="content-center header__bloco__contato bg_lightblue">
		<div class="row align-center height-full pd-0_20 wrap">					
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 flex">
				<a href="<?php the_field('contato_mapa', 'option'); ?>" class="darkgreen" target="_blank">
					<span><?php the_field('contato_endereco', 'option'); ?></span>
				</a>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 flex just-end header__bloco__contato__numbers">
				<a href="tel:+55<?php the_field('contato_telefone', 'option'); ?>" class="darkgreen flex align-center">
					<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/phone.png" class="img-auto" alt="">
					<span class="telmsk"><?php the_field('contato_telefone', 'option'); ?></span>
				</a>
				<a href="https://api.whatsapp.com/send?phone=55<?php the_field('contato_celular', 'option');?>&text=Ol%C3%A1,%20vim%20atrav%C3%A9s%20do%20site%20de%20Ponto%20Brasil.%20Quero%20conhecer%20mais%20sobre%20o%20servi%C3%A7o%20prestado%20por%20voc%C3%AAs." class="darkgreen flex align-center" title="Mande mensagem via Whatsapp" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/whats.png" class="img-auto" alt="">
					<span class="celmsk"><?php the_field('contato_celular', 'option'); ?></span>
				</a>
			</div>		
		</div>
	</div>

	<div class="bloco-bug hide"></div>

	