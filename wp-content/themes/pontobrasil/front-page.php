<?php get_header(); ?> 

	<main class="main" role="main">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<div class="container-fluid">
					
					<div class="home__banner">
						<?php  include('dist/slickslider/home__banner.php');  ?>	
					</div>

					<div class="row">
						<div class="bg_lightblue width-full">
					    
					    	<div class="content-center">

						        <div class="box flex just-center home__banner__mini__container">
					        		<?php 
					        		if( have_rows('opcao_planos_destaque', 'option') ): 
					        			while ( have_rows('opcao_planos_destaque', 'option') ) : the_row(); 
					        		?>

					        	    	<div class="home__banner__mini">
				        		    		<img src="<?php the_sub_field('opcao_planos_destaque_imagem'); ?>" class="b-shadow" alt="">

				        		    		<div class="home__banner__mini__text b-shadow flex align-center just-center direction-col">
				        		    			<h2 class="flex align-center"><?php echo the_sub_field('opcao_planos_destaque_titulo'); ?></h2>		
				        		    			<p><?php the_sub_field('opcao_planos_destaque_descricao'); ?></p>
				        		    			<a href="#planos">
				        		    				<div class="button-lg green br_green flex align-center just-center">CONHEÇA NOSSOS PLANOS</div>
				        		    			</a>
				        		    		</div>
					        		    </div>								       

					        		<?php 
					        			endwhile; 
					        		endif; 
					        		?>    	
						   		</div>

						   		<div id="servicos" class="box home__servicos__container">
									<div class="row just-center mg-b_70">
										<div class="col-xs-12 col-sm-10 col-md-7 col-lg-6 home__servicos__text">
											<h2 class="txt-center pd-b_45"><?php the_field('servicos_titulo', 'option'); ?></h2>
											<?php the_field('servicos_linha_fina', 'option'); ?> 		
										</div>
									</div>					   			

									<div class="row just-center wrap home__servicos__icons">
										<?php 
										if( have_rows('servicos_todos', 'option') ): 
											while ( have_rows('servicos_todos', 'option') ) : the_row(); 
										?>

									    	<div class="col-20 flex direction-col align-center">
									    		<img src="<?php the_sub_field('servicos_icone'); ?>" class="img-auto" alt="">
									    		<span class="blue flex align-center"><?php the_sub_field('servico_nome'); ?></span>
									    	</div>									       

										<?php 
											endwhile; 
										endif; 
										?>
									</div>
						   		</div>

						   		<?php include('dist/slickslider/planos.php'); ?>

						   		<?php include('dist/slickslider/espaco.php'); ?>
						   		
						   		<div id="facilidades" class="home__facilidades__container">
						   			<div class="row just-center pd-b_45">
					   					<div class="col-xs-12 col-sm-10 col-md-7 col-lg-6">
					   						<h2 class="txt-center pd-b_45"><?php the_field('facilidades_titulo', 'option'); ?></h2>
					   						<?php the_field('facilidades_linha_fina', 'option'); ?>
					   					</div>		
					   				</div>
					   			</div>					   							   	
					   		</div>
					   	</div>
					   	
				   		<div class="box home__facilidades__mapa">
				   			<div class="row pd-b_45">
			   					<?php include("dist/slickslider/facilidades.php"); ?>				   				
				   			</div>	

				   			<div class="content-center">
					   			<div class="row bg_white just-center home__facilidades_list wrap">
				   					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2 flex align-center blue home__facilidades__icon">
				   						<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/local_ponto_brasil.png" class="img-auto" alt="Ponto Brasil Coworking">Ponto Brasil				
				   					</div>
				   					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2 flex align-center blue home__facilidades__icon">
				   						<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/local_restaurantes.png" class="img-auto" alt="Restaurantes">Restaurantes				
				   					</div>
				   					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2 flex align-center blue home__facilidades__icon">
				   						<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/local_mc.png" class="img-auto" alt="McDonald's Americana">McDonald's			
				   					</div>
				   					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2 flex align-center blue home__facilidades__icon">
				   						<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/local_farmacia.png" class="img-auto" alt="Farmácia">Farmácia			
				   					</div>

				   				</div>

				   				<div class="row bg_white just-center home__facilidades_list wrap">
				   					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2 flex align-center blue home__facilidades__icon">
				   						<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/local_smart.png" class="img-auto" alt="Shopping Center Smart Mall">Shopping Center				
				   					</div>
				   					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2 flex align-center blue home__facilidades__icon">
				   						<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/local_forum.png" class="img-auto" alt="Fórum Americana">Fórum				
				   					</div>
				   					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2 flex align-center blue home__facilidades__icon">
				   						<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/local_habbibs.png" class="img-auto" alt="Habib's">Habib's
				   					</div>
				   					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2 flex align-center blue home__facilidades__icon">
				   						<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/local_posto.png" class="img-auto" alt="Posto de combustível">Posto de combustível			
				   					</div>
					   			</div>

					   			<div class="row bg_white just-center home__facilidades_list wrap">
				   					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2 flex align-center blue home__facilidades__icon">
				   						<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/smart_fit.png" class="img-auto" alt="Academia Smart Fit">Smart Fit
				   					</div>
				   					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2 flex align-center blue home__facilidades__icon">
				   						<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/oab-americana.png" class="img-auto" alt="OAB Americana">OAB				
				   					</div>
				   					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2 flex align-center blue home__facilidades__icon">
				   						<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/colegio_politec.png" class="img-auto" alt="Colégio Politec Americana">Politec			
				   					</div>
				   					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2 flex align-center blue home__facilidades__icon">
				   						<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/lava_rapido.png" class="img-auto" alt="Lava Rápido">Lava Rápido			
				   					</div>
					   			</div>


				   			</div>

					   	</div>

					   	<div class="width-full bg_white">
						   	<div class="content-center">

						    	<div id="blog" class="box home__blog__container">

						    		<?php
							    		$blogID = 148;
							    		$blog = get_post($blogID); 
							    		$content = apply_filters('the_content', $blog->post_content); 							    	
						    		?>

						        	<div class="row just-center pd-b_45">
							            <div class="col-xs-12 col-sm-10 col-md-6 col-lg-6">
					   						<h2 class="txt-center pd-b_45"><?php echo get_the_title(148); ?></h2>
					   						<?php echo $content; ?>
							          	</div>
							        </div>

							        <div class="row home__blog__thumb just-center align-start">

							        	<?php
					        		    	$array_posts = array ( 
					        		    		'post_type'			=> 'post',
					        		    	  	'posts_per_page'	=> 3,
					        		    	);

					        		    	$blog_post = new WP_Query( $array_posts );

			        						if ( $blog_post->have_posts() ):
			        						    while ( $blog_post->have_posts() ) :
			        						    	$blog_post->the_post();	  

			        						    	echo '<a href="'.get_permalink().'" class="col-xs-12 col-sm-12 col-md-4 col-lg-4 flex direction-col just-center">';
		        									  
		        									  	echo '<div class="zoom_img">';        									  	
												  			echo '<img src="' . get_the_post_thumbnail_url(get_the_ID(), 'thumbnail') . '" class="img-auto" alt="">';
												  		echo '</div>';

												  		echo '<h3>';
												  			the_title();
												  		echo '</h3>';

													echo '</a>';				  

			        						    endwhile;
			        						endif;

							        	?>
							        </div>

							        <div class="row">
							        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 flex just-center">
							        		<a href="<?php echo the_permalink(148); ?>">
							        			<div class="button-lg blue br_blue flex align-center just-center">Ver todos os posts</div>
							        		</a>
							        	</div>
							        </div>
						    	</div>

						    	<div id="contato" class="box home__contato__container">
						        	<div class="row just-center pd-b_45">
						        		<div class="col-xs-12 col-sm-10 col-md-6 col-lg-6">
					   						<h2 class="txt-center pd-b_45">Contato</h2>
					   						<p class="blue mg-0 txt-uppercase">Preencha o formulário que nossa equipe entrará em contato diretamente com você.</p>	
							          	</div>
						        	</div>

						        	<div class="row just-center home__contato__form">
						        		<div class="col-lg-12">
						        			<?php echo do_shortcode('[contact-form-7 id="31" title="Contato"]'); ?>
						        		</div>
						        	</div>

						        </div>

						    </div>
					    </div>
					
					</div>					 

				</div>
			
			</div>
			
		<?php endwhile; endif; ?>

	</main>



<?php get_footer(); ?>