<?php get_header(); ?> 

	<main class="main template-default" role="main">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<div class="container-fluid">

					<div class="row">
						<?php if ( has_post_thumbnail() ) { ?>

								<img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), ''); ?>" class="single_thumb_full">

								<div class="content-center content-page content-page-thumb">	
						<?php } 
							else{ ?>

								<div class="content-center content-page">

						<?php } ?>						
					    
						        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

						        		<h1 class="txt-center"><?php the_title(); ?></h1>

						        		<?php the_content(); ?>				        			
				        			
						        	</div>	

							    </div>

					</div>

				</div>			
			
			</div>
			
		<?php endwhile; endif; ?>

	</main>

<?php get_footer(); ?>