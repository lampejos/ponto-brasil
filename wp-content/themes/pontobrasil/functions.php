<?php
include(TEMPLATEPATH . '/functions/_settings.php');
include(TEMPLATEPATH . '/functions/_security.php');
include(TEMPLATEPATH . '/functions/_performance.php');
include(TEMPLATEPATH . '/functions/_login.php');
include(TEMPLATEPATH . '/functions/acf.php');
include(TEMPLATEPATH . '/functions/assets.php');
include(TEMPLATEPATH . '/functions/images.php');
include(TEMPLATEPATH . '/functions/menus.php');
include(TEMPLATEPATH . '/functions/theme.php');
include(TEMPLATEPATH . '/functions/pagination.php');
include(TEMPLATEPATH . '/functions/cpt.php');
include(TEMPLATEPATH . '/dist/slickslider/slider_functions.php');
include(TEMPLATEPATH . '/dist/exit_popup/modal_functions.php');