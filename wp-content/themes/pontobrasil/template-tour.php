<?php 
	/* Template Name: Tour Virtual*/
	get_header(); 
?> 

	<main class="main" role="main">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<div class="container-fluid">

					<div class="row">						
					    
				    	<div class="content-center content-page tour__content">					        				    					  
				        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				        		<h1 class="txt-center"><?php the_title(); ?></h1>

				        		<?php the_content(); ?>

				        		<div class="row align-start tour__content-iframe">
									<?php echo get_field('contato_tour_virtual', 'option'); ?>
									<div class="block-tour"><span>Click aqui para ativar o tour virtual</span></div>
				        		</div>
				        	</div>					        
					    </div>

					</div>
				
				</div>			
			
			</div>
			
		<?php endwhile; endif; ?>
	</main>

<?php get_footer(); ?>