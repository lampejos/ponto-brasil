<?php get_header(); ?> 

	<main class="main" role="main">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<script type="text/javascript">
				var ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
				var page = 2;
				jQuery(function($) {
				    $('body').on('click', '#more_posts', function() {
				        var data = {
				            'action': 'load_posts_by_ajax',
				            'page': page,
				            'security': '<?php echo wp_create_nonce("load_more_posts"); ?>'
				        };
				 
				        $.post(ajaxurl, data, function(response) {
				            $('.my-posts').append(response);
				            page++;
				        });
				    });
				});
			</script>


			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<div class="container-fluid">

					<div class="row">						
					    
				    	<div class="content-center content-page">					        				    					  
				        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				        		<h1 class="txt-center mg-b_40"><?php the_title(); ?></h1>

				        		<?php
				        			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

				        			$array_posts = array ( 
				        				'post_type'			=> 'post',
				        			  	'posts_per_page'	=> 1,
				        			);

				        			$blog_last_post = new WP_Query( $array_posts );

				        			if ( $blog_last_post->have_posts() ):
				        			    while ( $blog_last_post->have_posts() ) : $blog_last_post->the_post();
				        			    	
				        					echo '<a href="' .get_permalink(). '">';
									        	echo '<img src="' . get_the_post_thumbnail_url(get_the_ID(), 'large') . '" alt="" class="page-thumb">';
									        echo '</a>';


									        echo '<a href="' .get_permalink(). '">';
										        echo '<div class="blog_last_post">';

										        	echo '<div class="flex">';
				  		        				  		foreach((get_the_category()) as $category) { 	        			        		
				  											echo '<span class="post-categories">' .$category->cat_name. '</span>'; 
				  		        			        	}
				  		        			        echo '</div>';

										        	echo '<h2>';
										        		the_title();
										        	echo '</h2>';

										        	echo '<p class="txt-uppercase">' . wp_trim_words( get_the_content(), 35, '.' ) . '</p>';

										        	echo '<a href="'.get_permalink().'">';
										        		echo '<div class="button-sm blue br_blue inl-flex just-center align-center">Leia Mais</div>';
										        	echo '</a>';

										        echo '</div>';
									        echo '</a>';

				        			    endwhile;
				        			endif;
				        		?>

				        		<div class="row blog__thumb__post align-start">
				        	
				        			<?php
		        				    	$args = array( 'numberposts' => '1' );
		        						$recent_posts = wp_get_recent_posts( $args );
		        						$id_arr = [];
		        						foreach( $recent_posts as $recent ){
		        							$id_arr[] = $recent_posts[0]['ID'];
		        						}	

		        						$array_posts = array ( 
		        							'post_type'			=> 'post',
		        						  	'posts_per_page'	=> 6,
		        						  	'paged'				=> $paged,
		        						  	'offset'			=> '1',
		        						  	// 'post__not_in'		=> $id_arr
		        						);

		        						$blog_post = new WP_Query( $array_posts );

		        						if ( $blog_post->have_posts() ):
		        						    while ( $blog_post->have_posts() ) :
		        						    	$blog_post->the_post();

		        						    	echo '<a href="'.get_permalink().'" class="col-xs-12 col-sm-6 col-md-4 col-lg-4 flex direction-col just-center pd-b_60 blog__thumb__single">';
	        									  	 

	        									  	echo '<div class="zoom_img">';        									  	
											  			echo '<img src="' . get_the_post_thumbnail_url(get_the_ID(), 'thumbnail') . '">';

											  		echo '</div>';

											  		echo '<div class="flex">';
				  		        				  		foreach((get_the_category()) as $category) { 	        			        		
				  											echo '<span class="post-categories">' .$category->cat_name. '</span>'; 
				  		        			        	}
				  		        			        echo '</div>';										        	

											  		echo '<h3>';
											  			the_title();
											  		echo '</h3>';
											  		
											  		echo '<div class="flex align-end">';
											        	echo '<div class="button-sm blue br_blue flex align-center just-center">Leia Mais</div>';
										        	echo '</div>';

												echo '</a>';				  

		        						    endwhile;
		        						endif;
				        			?>

				        		</div>

								<div class="row blog__thumb__post align-start my-posts"></div>
				        		
				        		
				        		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 flex just-center mg-t_20">
				        			<div id="more_posts">
				        				<div class="button-md blue br_blue flex align-center just-center">Mais notícias</div>
				        			</div>
				        		</div>


				        	</div>					        
					    </div>

					</div>

					
					<?php  include('dist/slickslider/planos.php');  ?>

				</div>			
			
			</div>
			
		<?php endwhile; endif; ?>

	</main>

<?php get_footer(); ?>