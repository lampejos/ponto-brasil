	<footer class="footer" role="contentinfo">
		<div class="row footer__container bg_lightblue">    
	    	<div class="content-center">	    	

				<div class="row footer__social">
		    		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 footer__social__logo">
		    			<img src="<?php echo get_template_directory_uri(); ?>/dist/images/logo-pontobrasil_mini.png" class="img-auto">
		    		</div>

		    		<div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
		    			<p class="blue">Endereço</p>
		    			<a href="<?php the_field('contato_mapa', 'option'); ?>" class="darkgreen" target="_blank">
		    				<span class="blue"><?php the_field('contato_endereco', 'option'); ?></span>
		    			</a>
		    		</div>

		    		<div class="col-xs-offset-1 col-xs-12 col-sm-4 col-md-3 col-lg-2 footer__social__contato">
		    			<p class="blue">Fale Conosco</p>
		    			<a href="tel:+55<?php the_field('contato_telefone', 'option'); ?>" class="flex align-center">
		    				<svg version="1.1" id="tel1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 10 10" style="enable-background:new 0 0 10 10;" xml:space="preserve" width="10" height="10">		    			
		    					<path class="blue_fill" d="M6.4,7.4C6.5,7.2,7,6.4,7.8,6.6c0.8,0.3,2.9,1,1.9,2.3c-3.3,4.4-13-5.4-8.6-8.6c1.3-0.9,2,1.1,2.3,1.9
		    					S2.8,3.5,2.6,3.6C2,4.1,5.9,8,6.4,7.4z"/>
		    				</svg>
		    				<span class="blue telmsk"><?php the_field('contato_telefone', 'option'); ?></span>
		    			</a>
		    			<a href="https://api.whatsapp.com/send?phone=55<?php the_field('contato_celular', 'option');?>&text=Ol%C3%A1,%20vim%20atrav%C3%A9s%20do%20site%20de%20Ponto%20Brasil.%20Quero%20conhecer%20mais%20sobre%20o%20servi%C3%A7o%20prestado%20por%20voc%C3%AAs." class="flex align-center">
		    				<svg version="1.1" id="tel2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 13 13" style="enable-background:new 0 0 13 13;" xml:space="preserve" width="13" height="13">		    			
			    				<g>
			    					<path id="WhatsApp" class="blue_fill" d="M13,6.3c0,3.5-2.9,6.3-6.4,6.3c-1.1,0-2.2-0.3-3.1-0.8L0,13l1.2-3.4c-0.6-1-0.9-2.1-0.9-3.3
			    						C0.2,2.8,3.1,0,6.6,0C10.1,0,13,2.8,13,6.3z M6.6,1c-3,0-5.4,2.4-5.4,5.3c0,1.2,0.4,2.2,1,3.1l-0.7,2l2.1-0.7
			    						c0.8,0.6,1.9,0.9,3,0.9c3,0,5.4-2.4,5.4-5.3S9.6,1,6.6,1z M9.8,7.8c0-0.1-0.1-0.1-0.3-0.2C9.4,7.5,8.6,7.2,8.5,7.1
			    						C8.3,7.1,8.2,7,8.1,7.2C8,7.3,7.7,7.7,7.6,7.8c-0.1,0.1-0.2,0.1-0.3,0C7.1,7.8,6.6,7.6,6,7.1C5.6,6.6,5.2,6.1,5.2,6
			    						c-0.1-0.2,0-0.2,0.1-0.3c0.1-0.1,0.2-0.2,0.2-0.3c0.1-0.1,0.1-0.2,0.2-0.3c0.1-0.1,0-0.2,0-0.3c0-0.1-0.4-0.8-0.5-1.2
			    						C5,3.4,4.9,3.5,4.8,3.5c-0.1,0-0.2,0-0.3,0c-0.1,0-0.3,0-0.4,0.2C3.9,3.8,3.5,4.2,3.5,4.9c0,0.8,0.6,1.5,0.6,1.6
			    						c0.1,0.1,1.1,1.7,2.7,2.3c1.6,0.6,1.6,0.4,1.9,0.4c0.3,0,0.9-0.4,1.1-0.7C9.9,8.2,9.9,7.9,9.8,7.8z"/>
			    				</g>
		    				</svg>
		    				<span class="blue celmsk"><?php the_field('contato_celular', 'option'); ?></span>
		    			</a>
		    		</div>

		    		<div class="col-xs-12 col-sm-3 col-md-2 col-lg-4 flex just-end footer__social__redes">

		    			<?php if( have_rows('contato_redes_sociais', 'option') ):
		    			    	while ( have_rows('contato_redes_sociais', 'option') ) : the_row(); ?> 		    			
		    					    			    
			    			     	<a href="<?php the_sub_field('contato_redes_sociais_link'); ?>" title="<?php the_sub_field('contato_redes_sociais_nome'); ?>" target="_blank">
			    			        	<img src="<?php the_sub_field('contato_redes_sociais_icone'); ?>" alt="<?php the_sub_field('contato_redes_sociais_nome'); ?>">
			    			        </a>

		    			<?php endwhile; endif; ?>
		    			
		    		</div>
	    		</div>

	    		<div class="row footer__institucional">
	    			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
	    				<span class="blue">© Ponto Brasil - Todos os direitos reservados</span>
	    			</div>

	    			<div class="col-xs-12 col-sm-9 col-md-4 col-lg-4 txt-center">
	    				<a href="<?php echo get_permalink(276); ?>" class="blue" title="Termos de Uso">Termos de Uso |</a>
	    				<a href="<?php echo get_permalink(270); ?>" class="blue" title="Política de Privacidade">Política de Privacidade</a>
	    			</div>

	    			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-4 flex just-end">
	    				<a href="https://lampejos.com.br" title="Lampejos - Marketing Digital" target="_blank">
		    				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 78 19" style="enable-background:new 0 0 78 19;" xml:space="preserve" width="78" height="19">
			    				<g>
			    					<path class="blue_fill" d="M0.1,11.7L2.4,0h2.4L2.5,11.7C2.3,12.6,2.7,13,3.2,13c0.2,0,0.4,0,0.6-0.1L4,14.6C3.6,14.8,3,15,2.3,15
			    						C0.6,15-0.3,13.8,0.1,11.7z"/>
			    					<path class="blue_fill" d="M11.6,14c-0.6,0.6-1.4,1.1-2.8,1.1c-2.3,0-3.7-1.8-3.2-4.5L6.2,8c0.5-2.7,2-4.3,4.4-4.3c1.1,0,1.9,0.5,2.4,1.1l0.7-0.9h1.9
			    						L14,11.7C13.8,12.6,14,13,14.5,13c0.2,0,0.4,0,0.6-0.1l0.2,1.7c-0.4,0.2-1.1,0.4-1.8,0.4C12.5,15,11.9,14.7,11.6,14z M12.5,6.9
			    						c-0.4-0.5-1-0.9-1.7-0.9c-1.3,0-2,0.7-2.3,2.3l-0.5,2.3c-0.3,1.5,0.1,2.3,1.4,2.3c0.7,0,1.5-0.4,2.1-0.9L12.5,6.9z"/>
			    					<path class="blue_fill" d="M25,14.9h-2.4L24,8.1c0.3-1.5-0.2-2-1.3-2c-0.8,0-1.5,0.4-2.1,0.9l-1.5,8h-2.4l2.1-11h2l0,1.2c0.6-0.6,1.8-1.3,3.2-1.3
			    						c1.4,0,2.2,0.7,2.6,1.4c0.8-0.6,1.7-1.4,3.2-1.4c2.2,0,3,1.7,2.6,3.9l-1.4,7.3h-2.4l1.3-6.8c0.3-1.5-0.3-2.1-1.3-2.1
			    						c-0.7,0-1.4,0.4-2,0.8c0,0.4,0,0.6-0.1,1.1L25,14.9z"/>
			    					<path class="blue_fill" d="M34.7,18.9h-2.4l2.9-15h1.9l0.1,1c0.7-0.6,1.8-1.2,2.9-1.2c2.4,0,3.3,1.8,2.8,4.3l-0.6,2.8c-0.5,2.7-1.8,4.3-4.2,4.3
			    						c-1.1,0-2.1-0.5-2.6-1.1L34.7,18.9z M36,12c0.4,0.5,1,0.9,1.7,0.9c1.3,0,2-0.7,2.3-2.3l0.4-2.3C40.7,6.9,40.3,6,39,6
			    						c-0.7,0-1.4,0.4-2,0.9L36,12z"/>
			    					<path class="blue_fill" d="M44.7,8c0.5-2.6,2.1-4.3,4.9-4.3c2.8,0,3.9,1.6,3.4,4.3l-0.4,2.3h-5.9l-0.2,1.1C46.2,12.2,46.6,13,48,13
			    						c1.1,0,1.9-0.3,3-0.8l0.6,1.8c-1.2,0.7-2.4,1.1-4,1.1c-2.8,0-4-1.5-3.6-3.9L44.7,8z M47,8.4h3.6c0.3-1.7,0.2-2.6-1.3-2.6
			    						C47.8,5.8,47.3,6.6,47,8.4z"/>
			    					<path class="blue_fill" d="M56.3,3.9h2.4l-2.2,11.3C56,17.5,54.9,19,52.3,19c-0.5,0-1.3-0.1-2-0.4l0.9-1.8c0.3,0.1,0.8,0.2,1,0.2c1,0,1.5-0.5,1.7-1.4
			    						L56.3,3.9z M57,0h2.6L59,2.6h-2.6L57,0z"/>
			    					<path class="blue_fill" d="M59.3,10.8l0.5-2.8c0.5-2.7,2.1-4.3,5-4.3c2.8,0,4.1,1.6,3.5,4.3l-0.6,2.8c-0.5,2.7-2.1,4.2-5,4.2
			    						C60,15.1,58.8,13.4,59.3,10.8z M62.2,8.2l-0.5,2.5c-0.3,1.5,0,2.3,1.4,2.3c1.4,0,2-0.7,2.3-2.3L66,8.2c0.3-1.6,0-2.3-1.4-2.3
			    						C63.1,5.9,62.5,6.6,62.2,8.2z"/>
			    					<path class="blue_fill" d="M68.7,14l1-1.8c1.2,0.6,2.2,0.8,3,0.8c1.4,0,2-0.7,2-1.6c0-1.7-4.4-1-4.4-4c0-2.5,1.6-3.7,4.2-3.7c1.3,0,2.7,0.4,3.5,1
			    						l-1.1,1.8c-1.1-0.6-1.9-0.7-2.5-0.7c-1.1,0-1.8,0.3-1.8,1.2c0,1.5,4.4,1,4.4,4c0,2.5-1.1,4.1-4.5,4.1C71.2,15.1,69.5,14.6,68.7,14z
			    						"/>
			    				</g>
		    				</svg>
	    				</a>
	    			</div>
	    		</div>

	    	</div>
		</div> 
	</footer>

	<?php wp_footer(); ?>
</body>
</html>